package com.skyshi.app.minispeedtrack;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.skyshi.app.camera2.R;
import com.skyshi.app.minispeedtrack.adapter.VideoGalleryAdapter;
import com.skyshi.app.minispeedtrack.adapter.VideoViewInfo;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by skyshi on 24/11/16.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MenuFragment extends Fragment implements View.OnClickListener{
    Button btn_start_record,btn_open_video;
    //Cursor cursor;
    //GridView listView;
    //Uri uri;
    //int column_index_data,column_index_folder_name,column_index_mime;
    //String absolutePathOfVideo =null, videoName, mimename;
    //ArrayList<VideoViewInfo> videoRows = new ArrayList<VideoViewInfo>();
    public FragmentTransaction ft;
    private static final String FRAGMENT_DIALOG = "dialog";
    private static final int REQUEST_PERMISSION = 1;
    private static final int GET_URI_VIDEO = 2;
    public static MenuFragment newInstance() {
        return new MenuFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn_start_record = (Button)view.findViewById(R.id.btn_start_record);
        btn_start_record.setOnClickListener(this);
        btn_open_video = (Button)view.findViewById(R.id.btn_open_video);
        btn_open_video.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_start_record:
                givePermission();
                ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.container, CameraFragment.newInstance(),"kamera");
                ft.addToBackStack(null);
                ft.commit();


                break;
            case R.id.btn_open_video:
                givePermission();
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                Uri uri = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getPath()+"Movies/minispeedtrack/");
                Log.d("path",uri.toString());
                intent.setDataAndType(uri, "*/*");
                startActivityForResult(intent,GET_URI_VIDEO);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==GET_URI_VIDEO){
            if(data!=null) {
                Log.d("URIDATA", data.getData().getPath().toString());
                Log.d("URIDATA", data.getData().toString());
                Intent toVideoView = new Intent(getActivity(), VideoViewRecording.class);
                toVideoView.putExtra("UriString", data.getData().toString());
                startActivity(toVideoView);
            }
        }
    }

    private void givePermission(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED||
            ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED||
            ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED||
            ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED||
            ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            FragmentCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSION);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                return;
            }
        } else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
