package com.skyshi.app.minispeedtrack.adapter;

/**
 * Created by skyshi on 24/11/16.
 */

public class VideoViewInfo {
    private String filePath;
    private String mimeType;
    private String thumbPath;
    private String title;

    public VideoViewInfo(String path,String title,String mimeType){
        super();
        this.filePath = path;
        this.title = title;
        this.mimeType = mimeType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
