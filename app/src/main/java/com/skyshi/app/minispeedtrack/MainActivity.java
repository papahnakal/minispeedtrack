package com.skyshi.app.minispeedtrack;

import android.app.FragmentTransaction;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.skyshi.app.camera2.R;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    CameraFragment cameraFragment;
    FragmentTransaction ft;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        if (null == savedInstanceState) {
            ft = getFragmentManager().beginTransaction()
            .replace(R.id.container, MenuFragment.newInstance(),"Menu");
            ft.addToBackStack(null);
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("backstack",getFragmentManager().getBackStackEntryCount()+"");
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            if(getFragmentManager().getBackStackEntryCount()==1){
                finish();
            }else {
                getFragmentManager().popBackStack();
            }
        }else {
            finish();
        }
    }
}
