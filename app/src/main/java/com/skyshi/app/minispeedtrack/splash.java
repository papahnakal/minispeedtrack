package com.skyshi.app.minispeedtrack;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.skyshi.app.camera2.R;

public class splash extends AppCompatActivity {
    private static boolean splashLoaded = false;
//    @NonNull @Override public SplashPresenter createPresenter() {
//        return new SplashPresenter();
//    }
//
//    @Override public void hide() {
//        Intent intent = new Intent(this, HomeActivityTab.class);
//        startActivity(intent);
//        finish();
//    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!splashLoaded) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_splash);
            int secondsDelayed = 1;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //presenter.hide();
                    Intent intent = new Intent(splash.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
            }, secondsDelayed * 1000);
            splashLoaded = true;
            //presenter.hide();
        }else{
            Intent goToMainTab = null;
            goToMainTab = new Intent(splash.this,MainActivity.class);
            goToMainTab.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(goToMainTab);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
}
