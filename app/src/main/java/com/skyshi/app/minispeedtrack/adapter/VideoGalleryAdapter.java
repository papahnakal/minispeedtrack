package com.skyshi.app.minispeedtrack.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyshi.app.camera2.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by skyshi on 24/11/16.
 */

public class VideoGalleryAdapter extends BaseAdapter{
    private Context context;
    private List<VideoViewInfo> videoItems;

    LayoutInflater inflater;

    public VideoGalleryAdapter(Context _context,
                               ArrayList<VideoViewInfo> _items) {
        context = _context;
        videoItems = _items;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return videoItems.size();
    }

    public Object getItem(int position) {
        return videoItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View videoRow = inflater.inflate(R.layout.row, null);
        ImageView videoThumb = (ImageView) videoRow
                .findViewById(R.id.ImageView);
        if (videoItems.get(position).getThumbPath() != null) {
            videoThumb.setImageURI(Uri
                    .parse(videoItems.get(position).getThumbPath()));
        }

        TextView videoTitle = (TextView) videoRow
                .findViewById(R.id.TextView);
        videoTitle.setText(videoItems.get(position).getTitle());

        return videoRow;
    }
}
